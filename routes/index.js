var express = require('express');
var http = require('http');
// var exec = require('exec');
const spawn = require('child_process').spawn;

var streamOptions = require('./../stream-settings')

var router = express.Router();

var fs = require('fs')
var staticPath = require('./../static-path');

var connections = 0;

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Remote Camera'});
});





setInterval(function() {
    console.log(connections);
},2000);


router.get('/stream', function(req, res) {

    console.log('/stream/ open');
    connections++;
  var boundary = "BoundaryString";
  var options = streamOptions;
  options.headers = req.headers;

  var streamReq = http.request(options, function(streamRes) {

      res.setHeader('Content-Type', 'multipart/x-mixed-replace;boundary="' + boundary + '"');
      res.setHeader('Connection', 'close');
      res.setHeader('Pragma', 'no-cache');
      res.setHeader('Cache-Control', 'no-cache, private');
      res.setHeader('Expires', 0);
      res.setHeader('Max-Age', 0);

      streamRes
      .on('close', function(){
                      // closed, let's end client request as well 
                      //res.writeHead(streamRes.statusCode);
                      //res.end();
                    })
      .on('end', function(){ });
    //streamRes.pipe(res,{ end: false });

    streamRes.on('data', function (chunk) {
            res.write(chunk);
    });

  })
  .on('error', function(e) {
                console.log(e.message);
                streamReq.end();
                // we got an error, return 500 error to client and log error
                var file = new fs.ReadStream(staticPath+'/images/no-avaliable.jpg');
                file.pipe(res);
                });

  streamReq.end();

  req.on('close', function(){
    res.end();
  });
  req.on('error', function(e) {
    console.log(e.message);
    res.end();
  });
  req.on('end', function() {
    streamReq.end();
    connections--;
  });

});

function getFormattedDate() {
    var date = new Date();
    var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "_" +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

    return str;
}

router.post('/video',function(req,res){

                    var now = getFormattedDate();
                    if (parseInt(req.body.time) && req.body.time == parseInt(req.body.time, 10)) {
                        if(parseInt(req.body.time, 10) <= 0) req.body.time = 1;
                        if(parseInt(req.body.time, 10) > 3600) req.body.time = 3600;
                        var time = req.body.time.toString();
                    }else{

                        var time = '10';
                    }
                    const ffmpegVideo = spawn('ffmpeg', 
                        [
                            '-f', 'mjpeg',
                            '-t', time,
                            '-i', 'http://'+streamOptions.host+':'+streamOptions.port,
                            '-vcodec', 'mpeg4',
                            '/mnt/dav/remotecam/video_'+now+'.mp4'
                        ],
                        {cwd: '/tmp/'});
                    ffmpegVideo.stdout.on('data', function(data){
                      console.log('stdout: '+data);
                    });
                    ffmpegVideo.stderr.on('data', function(data){
                      console.log('stderr: '+data);
                    });
                    ffmpegVideo.on('close', function (code) {
                      console.log('child process exited with code '+code);
                      res.end(code);
                    });
                    
});

router.post('/photo',function(req,res){   
                    var now = getFormattedDate();
                    const ffmpegPhoto = spawn('ffmpeg', 
                        [
                            '-f', 'mjpeg',
                            '-r', '24',
                            '-i', 'http://'+streamOptions.host+':'+streamOptions.port,
                            '-vframes', '1',
                            '/mnt/dav/remotecam/image_'+now+'.jpg'
                        ],
                        {cwd: '/tmp/'});
                    ffmpegPhoto.stdout.on('data', function(data){
                      console.log('stdout: '+data);
                    });
                    ffmpegPhoto.stderr.on('data', function(data){
                      console.log('stderr: '+data);
                    });
                    ffmpegPhoto.on('close', function (code) {
                      console.log('child process exited with code '+code);
                      res.end(code);
                    });
                    
});


module.exports = router;
